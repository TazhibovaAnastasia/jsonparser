public class MyAddress{
	private String country;
	private String city;
	private String street;
	private int streetNumber;
	private int apartmentNumber;
	
	public MyAddress(String country, String city, String street, int streetNumber, int apartmentNumber){
		this.country=country;
		this.city=city;
		this.street=street;
		this.streetNumber=streetNumber;
		this.apartmentNumber=apartmentNumber;
	}
	
	public String getCountry(){
		return this.country;
	}
	
	public String getCity(){
		return this.city;
	}
	
	public String getStreet(){
		return this.street;
	}
	
	public int getStreetNumber(){
		return this.streetNumber;
	}
	
	public int getApartmentNumber(){
		return this.apartmentNumber;
	}
}