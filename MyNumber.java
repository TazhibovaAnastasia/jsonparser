public class MyNumber{
	private String type;
	private String number;
	
	public MyNumber(String type, String number){
		this.type=type;
		this.number=number;
	}
	
	public String getType(){
		return this.type;
	}
	
	public String getNumber(){
		return this.number;
	}
}