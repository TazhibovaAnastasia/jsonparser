import java.util.List;

public class MyInfo{
	private String name;
	private String surname;
	private MyAddress address;
	private List<MyNumber> numbers;
	
	public void setFullName(String name, String surname){
		this.name=name;
		this.surname=surname;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getSurname(){
		return this.surname;
	}
	
	public void setAddress(String country, String city, String street, int streetNumber, int apartmentNumber){
		this.address=new MyAddress(country, city, street, streetNumber, apartmentNumber);
	}
	
	public void setNumbers(List<MyNumber> numbers){
		this.numbers=numbers;
	}
	
	public List<MyNumber> getNumbers(){
		return this.numbers;
	}
	
	public void printMyInfo(){
		System.out.println("name: " + getName());
		System.out.println("surname: " + getSurname());
		System.out.println("address:");
		System.out.println("country: " + this.address.getCountry());
		System.out.println("city: " + this.address.getCity());
		System.out.println("street: " + this.address.getStreet()+ " " + this.address.getStreetNumber());
		System.out.println("apartment: "+this.address.getApartmentNumber());
		System.out.println("numbers:");
		for(int i=0; i<this.getNumbers().size(); i++){
			System.out.println(this.getNumbers().get(i).getType() + ":" + this.getNumbers().get(i).getNumber());
		}
	}
}