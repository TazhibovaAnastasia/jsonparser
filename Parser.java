import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

import com.google.gson.Gson;

public class Parser {

	public static void main(String[] args) throws IOException {
		// 1. ������� json "���������� ��� ���" (������ ��������)
		// �������������� �����
		MyInfo myInfo=new MyInfo();
		myInfo.setFullName("Anastasia", "Tazhibova");
		myInfo.setAddress("Russia", "Volgograd", "Tkacheva", 33, 33);
		List<MyNumber> numbers=new ArrayList<MyNumber>();
		numbers.add(new MyNumber("home", "33-33-33"));
		numbers.add(new MyNumber("phone", "3-333-333-33-33"));
		numbers.add(new MyNumber("phone", "4-444-444-44-44"));
		myInfo.setNumbers(numbers);
		// ��������� � json
		Gson gson=new Gson();
		String jsonString=gson.toJson(myInfo);
		System.out.println("to json:\n" + jsonString);
		// ��������� �� json
		MyInfo myInfo1=(MyInfo)gson.fromJson(jsonString, MyInfo.class);
		System.out.println("\nfrom json:");
		myInfo1.printMyInfo();
		
		// 2. Json �� ���������
		HttpURLConnection urlConnection=null;
        BufferedReader reader=null;
        String json="";
        try{
        	// ����������� � ���������� � ����� json
            URL url=new URL("http://androiddocs.ru/api/friends.json");
            urlConnection=(HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            InputStream inputStream=urlConnection.getInputStream();
            StringBuffer buffer=new StringBuffer();
            reader=new BufferedReader(new InputStreamReader(inputStream));
            // ��������� json � ������
            String line;
            while((line=reader.readLine())!=null){
                buffer.append(line);
            }
            json=buffer.toString();
        }catch (Exception e){e.printStackTrace();}
        System.out.println("\njson from internet:");
        System.out.println(json);
        
        // 3. �������� � Json
        // ��������� �����������
        File f=new File("m.jpg");
        BufferedImage img=ImageIO.read(f);
        // ������������ ����������� � ������ ����
        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        ImageIO.write(img, "jpg", outputStream);
        outputStream.flush();
        byte[] imageInByte=outputStream.toByteArray();
        outputStream.close();
        // ��������� ����� ����������� � base64
		String base64Str=DatatypeConverter.printBase64Binary(imageInByte);
		// ���������� ������ � json
		jsonString=gson.toJson(base64Str);

		// �������� ������ �� json
		String jsonString1=gson.fromJson(jsonString, String.class);
		// ��������� ������ base64 � �����
		imageInByte=DatatypeConverter.parseBase64Binary(jsonString1);
		// ������� �����������
		ByteArrayInputStream inputStream=new ByteArrayInputStream(imageInByte);
		BufferedImage newImg=ImageIO.read(inputStream);
		ImageIO.write(newImg, "png", new File("m1.jpg"));
	}
}